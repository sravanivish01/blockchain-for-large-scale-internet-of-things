USE [BlockchainIIoTV1]
GO
CREATE TABLE [dbo].[DatasetInfo](
[datasetID] [int] IDENTITY(1,1) NOT NULL,
[datasetFileName] [nvarchar](500) NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[IoTData](
[uniqueID] [int] IDENTITY(1,1) NOT NULL,
[demandResponse] [float] NULL,
[area] [float] NULL,
[season] [float] NULL,
[energy] [float] NULL,
[cost] [float] NULL,
[pairNo] [float] NULL,
[distance] [float] NULL,
[prevHash] [nvarchar](500) NULL,
[hash] [nvarchar](500) NULL,
[isBlockChainGenerated] [bit] NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Payment](
[paymentID] [int] IDENTITY(1,1) NOT NULL,
[supplier] [nvarchar](50) NULL,
[customer] [nvarchar](50) NULL,
[datafilename] [nvarchar](50) NULL,
[amount] [float] NULL,
[hash] [nvarchar](max) NULL,
[prevHash] [nvarchar](max) NULL,
[isBlockChainGenerated] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[Role](
[roleID] [int] IDENTITY(1,1) NOT NULL,
[roleName] [nvarchar](50) NULL,
[canRole] [bit] NULL,
[canUser] [bit] NULL,
[canUploadDataset] [bit] NULL,
[canDisplayIIoTData] [bit] NULL,
[canAreaVsSeason] [bit] NULL,
[canEnergyVsCost] [bit] NULL,
[canEnergyVsDistance] [bit] NULL,
[canCostVsDistance] [bit] NULL,
[canCsvToDatabase] [bit] NULL,
[canPayment] [bit] NULL,
[canBlockchainGeneration] [bit] NULL,
[canBlockchainReport] [bit] NULL,
[canDisplaySmartContract] [bit] NULL,
CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED
(
[roleID] ASC
67
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS =
ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
1 Admin True True True True True True True True True True
True True True
2 Public False False False False False False False False False False
False False False
NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL
NULL NULL NULL
CREATE TABLE [dbo].[UserTable](
[userID] [int] IDENTITY(1,1) NOT NULL,
[userName] [nvarchar](50) NULL,
[emailid] [nvarchar](50) NULL,
[password] [nvarchar](50) NULL,
[contactNo] [nvarchar](50) NULL,
[isActive] [bit] NULL,
[roleID] [int] NOT NULL,
CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED
(
[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS =
ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

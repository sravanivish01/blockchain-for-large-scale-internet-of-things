class PaymentModel:
 # instance attribute
 def _init_(self, paymentID, supplier="", customer="", datafilename="", amount=0,
hash="", prevHash="", isBlockChainGenerated=0):
 self.paymentID=paymentID
 self.supplier=supplier
 self.customer = customer
 self.datafilename = datafilename
 self.amount = amount
 self.hash = hash
 self.prevHash = prevHash
 self.isBlockChainGenerated = isBlockChainGenerated

class IoTDataModel:
 def _init_(self, uniqueID=0, demandResponse="",area=0, season=0, energy=0, cost=0,
pairNo=0, distance=0, prevHash="", hash="", isBlockChainGenerated=0):
 self.uniqueID = uniqueID
 self.demandResponse = demandResponse
 self.area = area
 self.season = season
 self.energy = energy
 self.cost = cost
 self.pairNo = pairNo
 self.distance = distance
 self.prevHash = prevHash
 self.hash = hash
 self.isBlockChainGenerated = isBlockChainGenerated
